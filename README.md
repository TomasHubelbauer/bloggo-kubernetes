# Kubernetes

- [ ] Work through and document [Kubernetes the Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way)
- [ ] Check out http://jmoiron.net/blog/is-k8s-too-complicated/
- [ ] Check out http://blog.kubernetes.io/2018/03/kubernetes-1.10-stabilizing-storage-security-networking.html
- [ ] Check out http://kubernetesbyexample.com/
- [ ] Check out https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/
- [ ] Check out https://www.katacoda.com/courses/kubernetes
- [ ] Check out https://github.com/kelseyhightower/kubernetes-the-hard-way
- [ ] Check out https://medium.com/@JockDaRock/minikube-on-windows-10-with-hyper-v-6ef0f4dc158c for Minikube + Hyper-V guide
- [ ] Check out https://twitter.com/dankohn1/status/989956137603747840
- [ ] Check out https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/
- [ ] Check out http://okigiveup.net/a-tutorial-introduction-to-kubernetes/
- [ ] Check out https://www.hanselman.com/blog/HowToBuildAKubernetesClusterWithARMRaspberryPiThenRunNETCoreOnOpenFaas.aspx+
    - Await the parts to arrive
- [ ] Check out https://blog.alexellis.io/serverless-kubernetes-on-raspberry-pi/

## Related posts

- Gitkube ([GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-gitkube), [Bloggo post](http://hubelbauer.net/post/gitkube))

## Kubeflow

Machine learning toolkit for Kubernetes.

## EdX Course

https://www.edx.org/course/introduction-kubernetes-linuxfoundationx-lfs158x

## OpenFass Function Store

https://blog.alexellis.io/announcing-function-store/

## *Letter to Santa Cube*

- [The article](https://jpetazzo.github.io/2017/12/06/letter-to-santa-kube/)
- [Hacker News comments](https://news.ycombinator.com/item?id=15950014)
